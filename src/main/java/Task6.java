import java.util.Arrays;

public class Task6 {
    public static void main(String[] args) {

//        Napisz metodę, która odwraca daną tablicę liczb całkowitych.
//        swap([1,2,3]) = [3,2,1]

        int[] array = {1,2,3,4,5,6,7,8,9};
        System.out.println("swap("+ Arrays.toString(array)+") = "+Arrays.toString(arraySwap(array)));

    }
    private static int [] arraySwap(int [] array){
        int [] swappedArray = new int[array.length];
        int j=0;
        for (int i = array.length-1; i >= 0; i--) {
            swappedArray[i]=array[j];
            j++;
        }
        return swappedArray;
    }
}
