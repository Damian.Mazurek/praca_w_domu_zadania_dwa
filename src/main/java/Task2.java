import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) {

//        Napisz metodę, która zwraca element środkowy tablicy.
//        middleElement(([1, 2, 3]) = 2
//        middleElement(([1, 5, 3, 4]) = 5

        int [] array = {1,2,3};
        int [] newArray = {2,5,7,9,3,4};

        System.out.println("middleElement(("+ Arrays.toString(array)+") = "+returnMiddleElement(array));
        System.out.println();
        System.out.println("middleElement(("+ Arrays.toString(newArray)+") = "+returnMiddleElement(newArray));

    }
    private static int returnMiddleElement(int [] array){
        if(array.length%2==0){
            return array[(array.length-1)/2];
        }else{
            return array[array.length/2];
        }
    }
}
