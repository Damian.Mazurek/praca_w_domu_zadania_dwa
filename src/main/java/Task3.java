import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {

//        Zakładamy, że mamy 2 tablice 2 elementowe. Nasza metoda powinna zwrócić
//        nową tablicę, która jest połączona z dwóch tablic.
//        plusTwoArrays([-4, 4], [8, 2]) → [-4, 4, 8, 2]
//        plusTwoArrays([9, 1], [3, 10]) → [9, 1, 3, 10]
        int[] arrayOne = {-4, 4, 1, 2, 3, 4, 5};
        int[] arrayTwo = {6, 5, 4, 3, 2, 1, 8, 2};

        System.out.println("arrayAdd("+Arrays.toString(arrayOne)+", "+Arrays.toString(arrayTwo)+" -> \n"+Arrays.toString(arrayAdd(arrayOne, arrayTwo)));

    }

    private static int [] arrayAdd(int[] arrayOne, int[] arrayTwo) {
        int[] arrayThree = new int[arrayOne.length + arrayTwo.length];
        int k = 0;
        for (int i = 0; i < arrayOne.length; i++) {
            arrayThree[i] = arrayOne[i];
        }
        for (int j = arrayOne.length; j < arrayThree.length; j++) {
            arrayThree[j] = arrayTwo[k];
            k++;
        }

        return arrayThree;
        }
    }






