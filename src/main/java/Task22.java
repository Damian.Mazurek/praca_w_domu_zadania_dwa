import java.util.Scanner;

public class Task22 {
    public static void main(String[] args) {

//       Napisz funkcję, która odczytuje jako argument liczbę
//       i wypisuje liczbę dzielników.
        run(dataIn());
    }

    private static int dataIn() {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę do sprawdzenia: ");
        return in.nextInt();
    }

    public static int numberOfDividers(int number) {
        int numberOfDividers = 0;
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                numberOfDividers++;
            }
        }
        return numberOfDividers;
    }

    private static String printOut(int number) {
        String wordCheck;
        if (number > -10 && number < 10) {
            wordCheck = "Cyfra ";
        } else {
            wordCheck = "Liczba ";
        }
        return wordCheck;
    }

    private static String devidersPrintOut(int numberOfDeviders) {
        String devidersCheck;
        if (numberOfDeviders == 1) {
            devidersCheck = " dzielnik";
        } else if (numberOfDeviders > 1 && numberOfDeviders < 5) {
            devidersCheck = " dzielniki";
        } else devidersCheck = " dzielników";
        return devidersCheck;
    }

    private static void run(int number) {
        System.out.println(printOut(number) + number + " posiada " + numberOfDividers(number) + devidersPrintOut(numberOfDividers(number)));
    }
}
