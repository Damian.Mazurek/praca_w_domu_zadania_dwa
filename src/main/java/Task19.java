import java.util.InputMismatchException;
import java.util.Scanner;

public class Task19 {
    public static void main(String[] args) {

//        Napisz funkcję, która stwierdza, czy zadana jako parametr liczba całkowita jest
//        kwadratem pewnej liczby całkowitej. Liczby będące kwadratami liczb całkowitych
//        to 1, 4, 9, 16 itd. Wartością funkcji ma być prawda, jeśli liczba spełnia warunek oraz
//        fałsz w przeciwnym wypadku.
        run();
    }

    private static void numberCheck(int number) {
        boolean variable = false;
        for (int i = 0; i < number; i++) {
            if (i * i == number) {
                variable = true;
                break;
            }
        }
        if(number!=0) {
            System.out.println(variable);
        }
    }

    private static int dataIn() {
        int number = 0;
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Wprowadź liczbę do sprawdzenia: ");

            number = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Wprowadzono niepoprawną liczbę.");
        }
        return number;
    }

    private static void run() {
        numberCheck(dataIn());
    }
}
