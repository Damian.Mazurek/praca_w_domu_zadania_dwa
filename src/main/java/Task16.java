import java.util.Scanner;

public class Task16 {
    public static void main(String[] args) {
//        Napisz metodę, która ma trzy parametry formalne a, b, c będące liczbami
//        całkowitymi. Funkcja zwraca prawdę, jeśli zadane liczby są liczbami
//        pitagorejskimi oraz fałsz w przeciwnym wypadku. Liczby pitagorejskie spełniają
//        warunek: a*a+b*b=c*c.
        run();
    }

    private static int[] dataIn() {
        Scanner in = new Scanner(System.in);
        int[] array = new int[3];
        System.out.println("Podaj parametr a: ");
        array[0] = in.nextInt();
        System.out.println("Podaj parametr b: ");
        array[1] = in.nextInt();
        System.out.println("Podaj parametr c: ");
        array[2] = in.nextInt();
        return array;
    }

    private static boolean dataCheck(int[] array) {
        boolean variable = false;
        int a = array[0];
        int b = array[1];
        int c = array[2];
        if (a * a + b * b == c * c) {
            variable = true;
        }
        return variable;
    }

    private static void run() {
        printOut(dataCheck(dataIn()));
    }

    private static boolean printOut(boolean variable) {
        if (variable) {
            System.out.println(variable + " liczby są pitagorejskie");
        } else {
            System.out.println(variable + " liczby nie są pitagorejskie");

        }
        return variable;
    }
}
