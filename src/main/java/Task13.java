import java.util.*;

import static java.lang.Thread.sleep;

public class Task13 {
    public static void main(String[] args) throws InterruptedException {

//        Napisz metodę, która jako argument przyjmuje tablicę liczb. Jako wynik metoda
//        powinna zwrócić tablicę wszystkich liczb, które mają dokładnie dwa dzielniki.

//        System.out.println(Arrays.toString(arrayCreate(20)));
//        System.out.println(Arrays.toString(numbersWithTwoDividers(arrayCreate())));
        run();
    }

    private static int[] arrayCreate() {

        int[] array = new int[sizeIn()];
        Random rand = new Random();
        int bound = boundIn();
        for (int i = 0; i < array.length; i++) {

            array[i] = rand.nextInt(bound);
            while (array[i] == 0 || array[i] == 1) {
                array[i] = rand.nextInt(bound);
            }
        }
        return array;
    }

    private static int[] numbersWithTwoDividers(int[] array) {
        Queue<Integer> list = new LinkedList<>() {
        };
        int k = 0;
        for (int value : array) {
            boolean liczbaPierwsza = true;
            for (int j = 2; j < value; j++) {
                if (value % j == 0) {
                    liczbaPierwsza = false;
                    break;
                }
            }
            if (liczbaPierwsza) {
                list.add(value);
                k++;
            }
        }
        int[] newArray = new int[k];
        try {
            for (int i = 0; i < k; i++) {
                newArray[i] = list.poll();
            }
        } catch (NullPointerException e) {
            System.out.println("Brak elementów do przypisania do nowej tablicy.");
        }

        return newArray;
    }

    private static int sizeIn() {
        int size = 0;
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Podaj wielkość tabeli do stworzenia: ");
            size = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Wprowadziłeś niepoprawną wielkość tabeli!");
            sizeIn();
        }
        return size;
    }

    private static int boundIn() {
        int bound = 0;
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Podaj górny zakres liczb w tabeli: ");
            bound = in.nextInt();
            if(bound<=1){
                System.out.println("Zakres liczb zbyt mały do wyświetlenia wylosowanych liczb pierwszych.");
                boundIn();
            }
        } catch (InputMismatchException e) {
            System.out.println("Wprowadziłeś niepoprawny zakres liczb całkowitych!");
            boundIn();
        }
        return bound;
    }

    private static void run() throws InterruptedException {
        System.out.println("""
                Program losuje wprowadzoną ilość liczb\s
                z wprowadzonego zakresu liczb naturalnych
                oraz wypisuje na ekran liczby pierwsze z wcześniej wylosowanych.""");
        sleep(2000);
        System.out.println();
        System.out.println(Arrays.toString(numbersWithTwoDividers(arrayCreate())));
    }
}
