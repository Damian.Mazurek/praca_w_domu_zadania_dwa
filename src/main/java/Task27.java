import java.util.Scanner;

public class Task27 {
    public static void main(String[] args) {

//        Napisz metodę, która sprawdza czy wyraz jest peselem. Dla uproszczenia
//        sprawdzamy czy każdy znak jest cyfrą i czy wyraz ma 11 znaków.
//        isPesel(“87122508076”)

        run();
    }

    private static boolean isPesel(String pesel) {
        boolean isDigit=false;
        if (pesel.length() == 11) {
            for (int i = 0; i < pesel.length(); i++) {
                if(Character.isDigit(pesel.charAt(i))){
                    isDigit=true;
                }else{
                    isDigit=false;
                    break;
                }
            }
        }
        return isDigit;
    }
    private static String dataIn(){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj numer pesel: ");
        return in.next();
    }
    private static void run(){
        if(isPesel(dataIn())){
            System.out.println("Jest to numer pesel");
        }else{
            System.out.println("Nie jest to numer pesel");
        }
    }
}
