import java.util.Scanner;

public class Task21 {
    public static void main(String[] args) {

//        Napisz metodę, która zwraca maksymalną długość 2 stringów.
//        maxStringLength(“aaaa”,”sad”) zwróci 4.
//        maxStringLength(“aaksadui”,”aaa”) zwróci 8.

        run();
    }

    private static int lengthCheck(String[] array) {
        return Math.max(array[0].length(), array[1].length());
    }

    private static String[] dataIn() {
        String[] array = new String[2];
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pierwszy string do sprawdzenia: ");
        array[0] = in.next();
        System.out.println("Podaj drugi string do sprawdzenia: ");
        array[1] = in.next();
        return array;
    }

    private static void run() {
        System.out.println(lengthCheck(dataIn()));
    }
}
