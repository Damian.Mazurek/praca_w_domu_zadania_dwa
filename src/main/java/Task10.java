import java.util.Arrays;

public class Task10 {
    public static void main(String[] args) {

        String[] array = {"Napisz", "metodę", "która", "jako", "argument", "przyjmuje", "tablicę", "Stringów", ".", "Jako", "wynik", "ma",
                "zwracać", "tablice", "wszystkich", "słów", "które", "zawierają", "słowa", "5-literowe"};
        System.out.println("Tablica przyjmująca słowa 5-literowe z tablicy Stringów -> " + Arrays.asList(fiveLetterWords(array)));

    }

    private static String[] fiveLetterWords(String[] array) {
        int newArrayLength = 0;
        for (String length : array) {
            if (length.length() == 5) {
                newArrayLength++;
            }
        }

        String[] newArray = new String[newArrayLength];
        int j = 0;
        for (String string : array) {
            if (string.length() == 5) {
                newArray[j] = string;
                j++;
            }
        }
        return newArray;
    }
}

