import java.util.Scanner;

public class Task17 {
    public static void main(String[] args) {

//        Napisz metodę sprawdzającą czy liczba jest parzysta.

        run();
    }

    private static void run() {
        numberCheck(dataIn());
    }

    private static int dataIn() {
        System.out.println("Podaj liczbę do sprawdzenia: ");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

    private static void numberCheck(int number) {
        if (number % 2 == 0) {
            System.out.println("Liczba jest parzysta");
        } else {
            System.out.println("Liczba jest nieparzysta");
        }
    }
}
