import java.util.Scanner;

public class Task24 {
    public static void main(String[] args) {
//        Napisz funkcję, która stwierdza,
//        czy zadana jako parametr liczba całkowita jest
//        liczbą pierwszą.

        run();
    }
    private static int dataIn(){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę do sprawdzenia: ");
        return in.nextInt();
    }
    private static boolean dataCheck(int number){
        boolean checkResult = true;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                checkResult = false;
                break;
        }
        }        if(number==0||number==1) {
            checkResult=false;
        }
            return checkResult;
    }
    private static void printout(boolean result){
        if(result){
            System.out.println("Jest to liczba pierwsza");
        }else{
            System.out.println("Nie jest to liczba pierwsza");
        }
    }
    private static void run(){
        printout(dataCheck(dataIn()));
    }
}
