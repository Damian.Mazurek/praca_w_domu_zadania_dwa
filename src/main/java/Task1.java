import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {

//        Napisz metodę, która dla danej tablicy zwraca sumę pierwszego i ostatniego
//        elementu tablicy.
//        sumArray([1, 2, 3]) = 1+3 = 4


        int [] array = {1,2,3};

        System.out.println("sumArray("+Arrays.toString(array)+") = "+addElements(array));
    }
    private static int addElements(int []array){
        return array[0]+array[array.length-1];
    }
}
