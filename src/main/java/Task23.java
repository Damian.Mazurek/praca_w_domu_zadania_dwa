import java.util.Scanner;

public class Task23 {
    public static void main(String[] args) {

//        Napisz funkcję, która stwierdza,
//        czy zadana jako parametr liczba całkowita jest
//        sześcianem pewnej liczby naturalnej.
run();
    }
    private static boolean numberCheck(int number){
        boolean checkResult = false;
        for (int i = 0; i < number; i++) {
            if(i*i*i==number){
                checkResult=true;
                break;
            }
        }return checkResult;
    }
    private static void printResult(boolean result){
        if(result){
            System.out.println("Dana liczba jest sześcianem");
        }else{
            System.out.println("Dana liczba nie jest sześcianem");
        }
    }
    private static int dataIn(){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę do sprawdzenia: ");
        return in.nextInt();
    }
    private static void run(){
        printResult(numberCheck(dataIn()));
    }
}
