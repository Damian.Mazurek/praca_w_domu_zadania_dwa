import java.util.Scanner;

public class Task26 {
    public static void main(String[] args) {

//        Napisz metodę, która sprawdza
//        czy dany wyraz jako argument jest palindromem.

        run();
    }

    private static String dataIn() {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj wyraz do sprawdzenia: ");
        return in.next();
    }

    private static boolean dataCheck(String word) {
        StringBuilder newWord = new StringBuilder();
        for (int i = word.length() - 1; i >= 0; i--) {
            newWord.append(word.charAt(i));
        }

        System.out.println(newWord);

        return word.equals(newWord.toString());
    }

    private static void run() {
        System.out.println(dataCheck(dataIn()));
    }
}
