import java.util.Arrays;

public class Task8 {
    public static void main(String[] args) {

//        Napisz metodę, która zwraca posortowaną tablicę liczb.
//        mySort([4,1,9,15]) = [1,4,9,15]
        int[] array = {4, 1, 9, 15};
        System.out.println("mySort("+ Arrays.toString(array)+") = "+Arrays.toString(sortArray(array)));

    }

    private static int [] sortArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                if (array[j - 1] > array[j]) {
                    int zmienna = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = zmienna;
                }
            }

        }
        return array;
    }
}
