import java.util.Arrays;

public class Task4 {
    public static void main(String[] args) {

//        Napisz metodę, która zwraca sumę elementów tablicy całkowitej.
//        sum([1,2,3]) = 6
        int [] array = {1,2,3,4,5,6,7,8,9};

        System.out.println("sum("+ Arrays.toString(array)+") = "+arraySum(array));

    }
    private static int arraySum(int [] array){
        int sum = 0;
        for (int element:array
             ) {
            sum+=element;
        }
        return sum;
    }
}
