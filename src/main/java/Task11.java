import java.util.Arrays;

public class Task11 {
    public static void main(String[] args) {

//        Napisz metodę, która jako argument przyjmuje tablicę Stringów. Jako wynik ma
//        zwracać tablice tej samej długości, w której wyrazy są napisane małymi literami(
//        duże litery zamieniamy na małe).

        String [] array = {"Napisz", "metodę", "która", "jako", "argument", "przyjmuje", "tablicę", "Stringów.", "Jako", "wynik", "ma",
                "zwracać", "tablice", "tej", "samej", "długości", "w", "której", "wyrazy", "są", "napisane", "małymi", "literami","(",
                "duże", "litery", "zamieniamy", "na", "małe)"};

        System.out.println(Arrays.toString(lowerCaseArray(array)));
    }
    private static String [] lowerCaseArray(String [] array){
        String [] newArray = new String[array.length];
        int i= 0;
        for (String word:array)
        {
          newArray[i]=word.toLowerCase();
          i++;
        }
        return newArray;
    }
}
