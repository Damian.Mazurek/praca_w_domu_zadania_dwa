import java.util.Scanner;

public class Task20 {
    public static void main(String[] args) {

//        Napisz metodę, która zwraca sumę dwóch stringów.
//        sum(“aaa”,”bb”) powinno zwrócić “aaabb”

run();
    }

    private static String dataIn() {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pierwsze słowo do scalenia: ");
        String firstWord = in.next();
        System.out.println("Podaj drugie słowo do scalenia: ");
        String secondWord = in.next();

        return firstWord + secondWord;

    }
    private static void run(){
        System.out.println(dataIn());
    }
}
