import java.util.*;

public class Task15 {
    public static void main(String[] args) {

        //        Napisz metodę, która wyznaczy liczbę wystąpień liczby k w danym ciągu.
        //        numberOfElements([1,2,3,3,3,4,3],3) = 4 ( 3 występuje 4 razy w danym ciągu).
        //        numberOfElements([1,2,3,3,3,4,3],8) = 0 ( 8 nie występuje ani razu w danym ciągu).
        run();
    }

    private static void run() {
        System.out.println("Program sprawdza ile razy ciagu wystepuje dana cyfra");
        int isWorking = 0;
        while (isWorking != 4) {
            isWorking = queue();
        }
    }

    private static int queue() {
        Scanner in = new Scanner(System.in);
        Queue<Integer> queue = new LinkedList<>();
        int choice = 0;
        int digit = 0;
        while (choice != 4) {
            if (queue.size() > 0) {
                if (choice != 3) {
                    System.out.println("Elementy ciągu -> " + queue);
                }
            }
            System.out.println(
                    "1. Dodaj element ciągu\n" +
                            "2. Dodaj element, który chcesz sprawdzić\n" +
                            "3. Sprawdź ciąg\n" +
                            "4. Wyjdź");
            choice = in.nextInt();
            switch (choice) {
                case 1:
                    try {
                        System.out.print("Podaj element: ");
                        queue.add(in.nextInt());
                    } catch (InputMismatchException e) {
                        System.out.println("Wprowadziłeś/aś niepoprawny element");
                    }
                    break;
                case 2:
//                    try {
                    digit = getDigit();
//                        System.out.println("Podaj element: ");
//                        digit = in.nextInt();
//                    } catch (InputMismatchException e) {
//                        System.out.println("Wprowadziłeś/aś niepoprawny element");
//                    }
                    break;
                case 3:
//                    for (int element : queue) {
//                        if (element == digit) {
//                            counter++;
//                        }
//                    }
                    digitCounter(queue, digit);
//                    System.out.println(queue.toString() + "," + digit + " = " + counter);
                    break;
                case 4:
                    choice = 4;
                    break;
                default:
                    System.out.println("Dokonałeś złego wyboru.");
                    break;
            }
        }

        return choice;
    }

    private static int getDigit() {
        Scanner in = new Scanner(System.in);
        int digit = 0;
        try {
            System.out.println("Podaj element: ");
            digit = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Wprowadziłeś/aś niepoprawny element");
        }
        return digit;
    }

    private static void digitCounter(Queue<Integer> cos, int digitCheck) {
        int counter = 0;
        for (int element : cos) {
            if (element == digitCheck) {
                counter++;
            }
        }
        System.out.println(cos.toString() + "," + digitCheck + " = " + counter);
    }
}
