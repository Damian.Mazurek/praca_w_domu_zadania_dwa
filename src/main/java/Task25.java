
public class Task25 {
    public static void main(String[] args) {

//        Skorzystaj z wcześniejszej funkcji (zad.11.) do znalezienia liczby od 2 do 10000,
//        która ma największą liczbę dzielników.


        run();
    }

    private static int quantityOfDivaders() {
        int maxDevidersNumber = 0;
        int number = 0;
        for (int i = 2; i <= 10000; i++) {
            if (Task22.numberOfDividers(i) > maxDevidersNumber) {
                maxDevidersNumber = Task22.numberOfDividers(i);
                number = i;
            }
        }
        return number;
    }

    private static void run() {
        System.out.println("Liczba z największą ilością dzielników z zakresu od 2 do 10000 to " + quantityOfDivaders());
    }
}
