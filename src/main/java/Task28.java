import java.util.Arrays;

public class Task28 {
    public static void main(String[] args) {
run();
    }

    private static int[] invert(int[] array) {
        int[] invertedArray = new int[array.length];
        int k = array.length-1;
        for (int i = 0; i <= array.length - 1; i++) {
            invertedArray[k] = array[i];
            k--;
        }
        return invertedArray;
    }

    private static void run() {
        System.out.println(Arrays.toString(invert(numbers)));
    }
    private static int[] numbers = {1,2,45,5,6,7,88,100};

}
