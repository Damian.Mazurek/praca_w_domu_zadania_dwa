import java.util.*;

public class Task14 {
    public static void main(String[] args) {

//        Napisz metodę, która jako argument zawiera tablicę elementów typu boolean.
//        Metoda ma zwrócić nową tablicę, która zawiera tyle elementów true ile znajduje
//        się w bazowej tablicy.
//        newArray([false,true,true,false,false]) = [true,true]
//        newArray([true,true,true,true,false,false, true]) = [true,true,true,true,true]
        run();

//runTest();
    }

    private static Boolean[] arrayCreate(int size) {
        Boolean[] array = new Boolean[size];
        Random rand = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = rand.nextBoolean();
        }
        return array;
    }

    private static Boolean[] arrayCheck(Boolean[] array) {
        Queue<Boolean> queue = new LinkedList<>();
//        for (boolean elementCheck : array) {
//            if (elementCheck) {
//                queue.add(elementCheck);
//            }
//        }
        for (Boolean aBoolean : array) {
            if (aBoolean) {
                queue.add(true);
            }
        }
        Boolean[] newArray = new Boolean[queue.size()];
//        for (boolean newElement : newArray) {
//            newElement = queue.poll();
//        }
        for (int i = 0; i < queue.size(); i++) {
            newArray[i] = queue.peek();
        }
        return newArray;
    }

    private static int dataCheck() {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj rozmiar tabeli: ");
        return in.nextInt();
    }

    private static void run() {
        System.out.println(Arrays.toString(arrayCheck(arrayCreate(dataCheck()))));
    }

//    private static void runTest() {
//        System.out.println(Arrays.toString(arrayCheck(arrayCreateTest())));
//    }
//
//    private static Boolean[] arrayCreateTest() {
//        Boolean[] testArray =
//                {true, false, true, false, true, false, true};
//        return testArray;
//    }
}
