public class Task12 {
    public static void main(String[] args) {

//        Napisz metodę, która jako argument przyjmuje tablice Stringów. Jako wynik
//        metoda ma zwracać liczbę całkowitą, która oznacza najdłuższy wyraz w tablicy.

        String[] array = {"Napisz", "metodę", "która", "jako", "argument", "przyjmuje", "tablice", "Stringów.", "Jako", "wynik",
                "metoda", "ma", "zwracać", "liczbę", "całkowitą", "która", "oznacza", "najdłuższy", "wyraz", "w", "tablicy."};
        System.out.println("Najdłuższy wyraz tabeli ma "+longestWord(array)+" znaków");

    }

    private static int longestWord(String[] array) {
        String longestWord = "";
        for (String word : array) {
            if (word.length() > longestWord.length()) {
                longestWord = word;
            }
        }
        return longestWord.length();
    }
}
