import java.util.Arrays;

public class Task5 {
    public static void main(String[] args) {

//        Napisz metodę, która zwraca największy element tablicy całkowitej.
//        max([2,3,5,1,20,25]) = 25

        int [] array = {2,3,5,1,20,25,87,21,46,4,6,44};

        System.out.println("max("+ Arrays.toString(array)+") = "+biggestArrayElement(array));

    }
    private static int biggestArrayElement(int [] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                if(array[j-1]>array[j]){
                    int zmienna = array[j-1];
                    array[j-1]=array[j];
                    array[j]=zmienna;
                }
            }
        }
        return array[array.length-1];
    }
}
